# Git CLI common procedures

## Fetch a remote project branch into a local branch

### List all remote branches
`git branch -r`

### Checkout the remote branch into a local branch
`git checkout -b local_branch_name origin/remote_branch_name`

